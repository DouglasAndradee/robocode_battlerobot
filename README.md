# robocode_battleRobot
É um robô de batalha com uma certa agressividade.
Ele tenta traçar um trajético quadrático em loop e ao mesmo tempo escanear a localização do seu oponente.
## Seu ponto positivo
É que após localizado ângulo e distância do oponente, ele calcula os mesmos em relação a sua pópria distância e támbem calcúla a sua quantidade de energia, para que desta forma, quanto mais energia ele tiver, maior séra o dano do tiro. Se a energia estiver baixa, o dano do tiro também diminuirá. Ele ainda faz o mesmo quando há colisão entre os robôs.
Obtendo na maioria das vezes um bom bônus e damage de RAM.

### Seu ponto fraco
É que não consegui implemetar nada de interessante para quando ele bate na parede, e por isso resolvi tirar o método.
Também acho que ele pode ser melhorado na forma de se locomover, rastrear e atacar o inimigo.

#### O que gostei 
Trata-se pra mim de uma experiência completamente nova com game, "IA", machine learning etc.
O conjunto torna tudo muito excitante para aprendizado e a vontade de tá buscando muito mais na documentção e fora para poder obter um robô de batalha muito melhor. É viciante demais!



